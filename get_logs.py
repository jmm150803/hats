import os

#Tool for generating single .logs file from multiple logs by Jakub Janšta
#Use this, if you have Python installed or you want to compile this garbage on our own


def get_logs():
    path = os.getenv("APPDATA") + "\\Godot\\app_userdata\\HATS\\logs\\"
    logs_in_path = os.listdir(path)
    for i in range(len(logs_in_path)):
        if logs_in_path[i].split(".")[-1] == "log":
            f = open(path+logs_in_path[i], "r")
            logs_in_path[i] = [logs_in_path[i], f.read()]
            f.close()
    return logs_in_path


def format_logs(logs):
    single_log = "Last logs\n"
    for log in logs:
        single_log += "\n*" + log[0] + "*\n=====LOG START=====\n" + log[1] + "=====LOG END=====\n\n" 
    return single_log[:-2]


if __name__ == "__main__":
    print("Getting logs...")
    logs = get_logs()
    print("Generating single .logs file...")
    log = format_logs(logs)
    inp = input("Would you like to save the .logs file to your desktop? [Y/n] ")
    if inp.lower() == "y":
        d_path = os.path.join(os.environ["USERPROFILE"]) +  "\\Desktop\\godot.logs"
        print("Saving .logs file to desktop (" + d_path + ")... ")
        f = open(d_path, "w")
        f.write(log)
        f.close()
        print("Success! Now you can send the godot.logs to somebody else to deal with")
    os.system("pause")