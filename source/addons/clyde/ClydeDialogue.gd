extends Node

const Interpreter = preload('./interpreter/Interpreter.gd')

class_name ClydeDialogue

signal variable_changed(name, value, previous_value)
signal event_triggered(name)

# Folder where the interpreter should look for dialogue files
# in case just the name is provided.
var dialogue_folder = 'res://dialogues/'

var _interpreter

var active_dialoque = ""


# Load dialogue file
# file_name: path to the dialogue file.
#            i.e 'my_dialogue', 'res://my_dialogue.clyde', res://my_dialogue.json
# block: block name to run. This allows keeping
#        multiple dialogues in the same file.
func load_dialogue(file_name, block = null):
	var file = _get_file_path(file_name)
	active_dialoque = file
	file = _load_file(file)
	_interpreter = Interpreter.new()
	_interpreter.init(file)
	_interpreter.connect("variable_changed", self, "_trigger_variable_changed")
	_interpreter.connect("event_triggered", self, "_trigger_event_triggered")
	if block:
		_interpreter.select_block(block)
	
	load_stored_clyde_data_to_memory()
	load_player_data_to_memory()
	


# Start or restart dialogue. Variables are not reset.
func start(block_name = null):
	_interpreter.select_block(block_name)


# Get next dialogue content.
# The content may be a line, options or null.
# If null, it means the dialogue reached an end.
func get_content():
	return _interpreter.get_content()


# Choose one of the available options.
func choose(option_index):
	return _interpreter.choose(option_index)


# Set variable to be used in the dialogue
func set_variable(name, value):
	_interpreter.set_variable(name, value)


# Get current value of a variable inside the dialogue.
# name: variable name
func get_variable(name):
	return _interpreter.get_variable(name)


# Return all variables and internal variables. Useful for persisting the dialogue's internal
# data, such as options already choosen and random variations states.
func get_data():
	return _interpreter.get_data()


# Load internal data
func load_data(data):
	return _interpreter.load_data(data)


# Clear all internal data
func clear_data():
	return _interpreter.clear_data()


func _load_file(path) -> Dictionary:
	if path.get_extension() == 'clyde':
		var container = _load_clyde_file(path)
		return container as Dictionary
	
	var f := File.new()
	f.open(path, File.READ)
	var result := JSON.parse(f.get_as_text())
	f.close()
	if result.error:
		printerr("Failed to parse file: ", f.get_error())
		return {}
	
	return result.result as Dictionary


func _load_clyde_file(path):
	var data = load(path).__data__.get_string_from_utf8()
	var parsed_json = JSON.parse(data)
	
	if OK != parsed_json.error:
		var format = [parsed_json.error_line, parsed_json.error_string]
		var error_string = "%d: %s" % format
		printerr("Could not parse json", error_string)
		return null
	
	return parsed_json.result


func _get_file_path(file_name):
	var p = file_name
	var extension = file_name.get_extension()
	
	if (not extension):
		p = "%s.clyde" % file_name
	
	if p.begins_with('./') or p.begins_with('res://'):
		return p
	
	return dialogue_folder.plus_file(p)


func _trigger_variable_changed(name, value, previous_value):
	emit_signal("variable_changed", name, value, previous_value)


func _trigger_event_triggered(name):
	if "_" in name:
		print("Clyde special trigger: " + name)
		name = name.split("_", false)
		if not name.size() > 0:
			push_error("Invalid special trigger. Use CamelCase for non special triggers")
		
		match name[0]:
			"remember":
				if name.size() == 2:
					if name[1] == "all":
						store_clyde_data(get_data())
					
					elif name[1] == "variables":
						var data = get_stored_clyde_data()
						data["variables"] = get_data()["variables"]
						store_clyde_data(data)
					
					else:
						var variables = get_data()["variables"]
						if name[1] in variables.keys():
							var data = get_stored_clyde_data()
							data["variables"][name[1]] = variables[name[1]]
							store_clyde_data(data)
						else:
							push_error("Remember: Variable '" + name[1] + "' is not in Clyde memory and can't be saved")
				
				elif name.size() == 1:
					push_error("Remember: Not enough arguments")
				else:
					push_error("Remember: Too many arguments")
					
			"forget":
				if name.size() == 2:
					if name[1] == "all":
						store_clyde_data({})
					
					elif name[1] == "variables":
						var data = get_stored_clyde_data()
						data["variables"] = {}
						store_clyde_data(data)
					
					else:
						var data = get_stored_clyde_data()
						if data["variables"].erase(name[1]):
							store_clyde_data(data)
					
				elif name.size() == 1:
					push_error("Forget: Not enough arguments")
				else:
					push_error("Forget: Too many arguments")
			
			"clear":
				if name.size() == 2:
					if name[1] == "all":
						load_data({"access":{},"variables":{},"internal":{}})
					
					elif name[1] == "internal":
						var data = get_data()
						load_data({"access":{},"variables":data["variables"],"internal":{}})
					
					elif name[1] == "variables":
						var data = get_data()
						load_data({"access":data["access"],"variables":{},"internal":data["internal"]})
					
					else:
						var data = get_data()
						if name[1] in data["variables"].keys():
							data["variables"].erase(name[1])
							load_data(data)
						else:
							push_error("Variable '" + name[1] + "' is not in Clyde memory and can't be cleared")
				
				elif name.size() == 1:
					push_error("Clear: Not enough arguments")
				else:
					push_error("Clear: Too many arguments")
				
			"hat":
				if name.size() == 2:
					Globals.gimme_tree().get_root().get_node("Level/Player").change_hat(name[1])
				elif name.size() == 1:
					Globals.gimme_tree().get_root().get_node("Level/Player").change_hat()
				else:
					push_error("Hat: Too many arguments")
				
				var data = get_data()
				data["variables"]["activeHat"] = Globals.player_data["hat"]["activeHat"]
				load_data(data)
			
			"unlock":
				if name.size() == 2:
					if name[1] in Globals.allHats:
						if not name[1] in Globals.player_data["hat"]["acquiredHats"]:
							Globals.player_data["hat"]["acquiredHats"].append(name[1])
					else:
						push_error("Item '" + name[1] + "' cannot be unlocked (not implemented category or non existing item)")
				elif name.size() == 1:
					push_error("Unlock: Not enough arguments")
				else:
					push_error("Unlock: Too many arguments")
				load_player_data_to_memory()
			
			"lock":
				pass
				if name.size() == 2:
					if name[1] in Globals.allHats:
						while name[1] in Globals.player_data["hat"]["acquiredHats"]:
							Globals.player_data["hat"]["acquiredHats"].erase(name[1])
					else:
						push_error("Item '" + name[1] + "' cannot be unlocked (not implemented category or non existing item)")
				elif name.size() == 1:
					push_error("Lock: Not enough arguments")
				else:
					push_error("Lock: Too many arguments")
				load_player_data_to_memory()
			
			_:
				push_error("Invalid special trigger '" + name.join("_") + "'. Use CamelCase for non special triggers")
	
	else:
		emit_signal("event_triggered", name)


func get_stored_clyde_data():
	var data = AppData.read_file_as_JSON("clyde/" + active_dialoque.split("/")[-1] + ".data")[1]
	return data if data else get_data()


func store_clyde_data(data):
	print("Clyde data storing code: ", AppData.write_file_as_JSON("clyde/" + active_dialoque.split("/")[-1] + ".data", data))


func load_dict_to_variable_emory(dict):
	var current = get_data()
	for key in dict.keys():
		current[key] = dict[key]
	load_data(current)


func load_stored_clyde_data_to_memory():
	var data = AppData.read_file_as_JSON("clyde/" + active_dialoque.split("/")[-1] + ".data")
	if data[0] == OK:
		load_data(data[1])
	elif data[0] != 1:
		push_error("Clyde: load_stored_clyde_data_to_memory() error: " + str(data[0]))


func load_player_data_to_memory():
	var data = get_data()
	data["variables"]["activeHat"] = Globals.player_data["hat"]["activeHat"]
	for hat in Globals.allHats:
		data["variables"]["has"+hat] = true if hat in Globals.player_data["hat"]["acquiredHats"] else false
	load_data(data)
