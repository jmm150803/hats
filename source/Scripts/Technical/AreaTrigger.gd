extends Area2D

enum{Characters, Objects, Both}
export (int, "Characters", "Objects", "Both") var monitored
export (bool) var oneshot

var _used = false

signal trigger_entered
signal trigger_exited
signal trigger_event(entered)


func _on_AreaTrigger_body_entered(body):
	if monitored == Characters:
		if body is KinematicBody2D:
			if not _used:
				if oneshot:
					_used = true
				emit_signal("trigger_entered")
				emit_signal("trigger_event", true)
	
	elif monitored == Objects:
		if body is RigidBody2D:
			if not _used:
				if oneshot:
					_used = true
				emit_signal("trigger_entered")
				emit_signal("trigger_event", true)
	
	elif monitored == Both:
		if body is KinematicBody2D or body is RigidBody2D:
			if not _used:
				if oneshot:
					_used = true
				emit_signal("trigger_entered")
				emit_signal("trigger_event", true)


func _on_AreaTrigger_body_exited(body):
	if monitored == Characters:
		if body is KinematicBody2D:
			if not _used:
				if oneshot:
					_used = true
				emit_signal("trigger_exited")
				emit_signal("trigger_event", false)
	
	elif monitored == Objects:
		if body is RigidBody2D:
			if not _used:
				if oneshot:
					_used = true
				emit_signal("trigger_exited")
				emit_signal("trigger_event", false)
	
	elif monitored == Both:
		if body is KinematicBody2D or body is RigidBody2D:
			if not _used:
				if oneshot:
					_used = true
				emit_signal("trigger_exited")
				emit_signal("trigger_event", false)
