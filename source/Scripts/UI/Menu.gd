extends Control

func _ready():
	pass
	
func _on_ExitButton_pressed():
	print("quit")
	get_tree().quit()


func _input(event):
	if event.is_action_pressed("ui_accept"):
		_on_lvl_pressed()

func _on_StartButton_pressed():
# warning-ignore:return_value_discarded
	print("load: res://Scenes/Levels/Debug.tscn")
	get_tree().change_scene("res://Scenes/Levels/Debug.tscn")


func _on_lvl_pressed():
	var file = File.new()
	if file.file_exists($TextEdit.text):
		print("load: ", $TextEdit.text)
# warning-ignore:return_value_discarded
		get_tree().change_scene($TextEdit.text)
	else:
		$invalid.visible = true
