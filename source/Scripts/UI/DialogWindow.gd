extends ColorRect

var _dialogue

func _ready():
	_dialogue = ClydeDialogue.new()
	_dialogue.load_dialogue('res://example/test.clyde')

	_dialogue.connect("event_triggered", self, '_on_event_triggered')
	_dialogue.connect("variable_changed", self, '_on_variable_changed')
	print(_dialogue.get_data())


func _get_next_dialogue_line():
	var content = _dialogue.get_content()
	if not content:
		$line/items.hide()
		#$dialogue_ended.show()
		return

	if content.type == 'line':
		_set_up_line(content)
		$line/items.hide()
	else:
		_set_up_options(content)
		$line/items.show()


func _set_up_line(content):
	var speaker = content.get('speaker')
	if speaker != null:
		$line/speaker.show()
		$line/speaker.text = speaker + ":"
	else:
		$line/speaker.hide()
	$line/text.text = content.text


func _set_up_options(options):
	for c in $line/items.get_children():
		c.queue_free()

	$line/speaker.text = options.get('speaker') if options.get('speaker') != null else ''
	$line/text.text = options.get('name') if options.get('name') != null else ''

	var index = 0
	for option in options.options:
		var btn = Button.new()
		btn.text = "[" + str(index + 1) + "] " + option.label
		btn.connect("button_down", self, "_on_option_selected", [index])
		$line/items.add_child(btn)
		index += 1


func _on_option_selected(index):
	_dialogue.choose(index)
	_get_next_dialogue_line()

func _gui_input(event):
	if event is InputEventMouseButton and event.is_pressed():
		_get_next_dialogue_line()

func _input(event):
	if event is InputEventKey:
		if not event.pressed: return
		if event.scancode == KEY_ENTER:
			_get_next_dialogue_line()
		elif event.scancode >= 49 and event.scancode <= 57:
			_on_option_selected(event.scancode % 49) # TODO: Fix trigger next page

func _on_event_triggered(event_name):
	print("Event received: %s" % event_name)
		

func _on_variable_changed(variable_name, new_value, previous_value):
	print("variable changed: %s old %s new %s" % [variable_name, previous_value, new_value])

func _on_restart_pressed():
	print(_dialogue.get_data())
	$dialogue_ended.hide()
	_dialogue.start()
	_get_next_dialogue_line()
