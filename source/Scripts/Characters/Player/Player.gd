extends KinematicBody2D

#TODO: coyote timer, slope stopper (raycast collision normal angle?), better ground detection on round edges (testmove to the sides), camera, Globals data, snap na rampy

#public and editor settings
export (float, 0.01, 100) var speed_tiles_per_sec = 3
export (float, 0, 1) var horizontal_lerp_weight = 0.6
export (float, 0.01, 100) var max_jump_height = 2.5
export (float, 0.01, 100) var min_jump_height = 0.5
export (float, 0.01, 60) var sec_jump_duration = 0.5
export (float, 0, 60) var sec_jump_buffer = 0.15
export (float, 0, 60) var sec_coyote_time = 0.1
#changing value of Collision Safe Margin may be required if physics collisions appear buggy
export (float) var physics_push_bounce_force = 75

#private variables basen on public settings
var _gravity
var _max_jump_velocity
var _min_jump_velocity
var _lerp_weight
var _speed
var _push_bounce_force

#script global woking variables
var _velocity = Vector2(0,0)
var is_grounded = false
enum{IDLE, WALK, JUMP, LAND, FALL}
var state = IDLE
var floor_anle = 0

#subnode cache
onready var Lapse = $Lapse
onready var coyoteTimer = $CoyoteTimer
onready var jumpBufferTimer = $JumpBufferTimer


func _ready():
	add_to_group("Characters")
	recalculate_movement_settings()


func recalculate_movement_settings():
	_gravity = 2 * Globals.tile_unit_size * max_jump_height / pow(sec_jump_duration, 2)
	_max_jump_velocity = -sqrt(2 * _gravity * max_jump_height * Globals.tile_unit_size)
	_min_jump_velocity = -sqrt(2 * _gravity * min_jump_height * Globals.tile_unit_size)
	_lerp_weight = 1 - horizontal_lerp_weight
	_speed =  speed_tiles_per_sec * Globals.tile_unit_size
	_push_bounce_force = physics_push_bounce_force if physics_push_bounce_force > 0 else 0
	coyoteTimer.wait_time = sec_coyote_time
	jumpBufferTimer.wait_time = sec_jump_buffer


func _physics_process(delta):
	var direction = int(Globals.player_data["player"]["canMove"]) * (-Input.get_action_strength("left") + Input.get_action_strength("right"))
	#flip sprite
	if direction == 1:
		Lapse.flip_h = true
	elif direction == -1:
		Lapse.flip_h = false
	
	_velocity.x = lerp(_velocity.x, _speed * direction, _lerp_weight)
	
	if Input.is_action_just_pressed("jump"):
		jumpBufferTimer.start()
	
	if is_grounded:
		if not jumpBufferTimer.is_stopped() and Globals.player_data["player"]["canMove"]:
			jumpBufferTimer.stop()
			_velocity.y = _max_jump_velocity
			state = JUMP
	else:
		_velocity.y += _gravity * delta

	#variable jump cutoff
	if not Input.is_action_pressed("jump") and _velocity.y < _min_jump_velocity:
		_velocity.y = _min_jump_velocity
	
	
	#statemachine
	match state:
		IDLE: 
			if direction:
				state = WALK
			elif not is_grounded:
				state = FALL
			else:
				Lapse.animation = "idle"
			
		WALK:
			if not is_grounded:
				state = FALL
			elif not direction:
				state = IDLE
			else:
				Lapse.animation = "walk"
			
		FALL:
			Lapse.animation = "air_down"
			if is_grounded:
				state = LAND
		
		LAND:
			Lapse.animation = "jump_down"
			if Lapse.frame == 2:
				state = IDLE
		
		JUMP:
			Lapse.animation = "jump_up"
			if _velocity.y > 0 or Lapse.frame == 1:
				state = IDLE
			
				
	#moving
	var collision = move_and_collide(_velocity * delta, false)
	if collision:
		_velocity = _velocity.slide(collision.normal)
		if collision.get_collider() is RigidBody2D:
			collision.collider.apply_central_impulse(-collision.normal * physics_push_bounce_force)
		var remainder = collision.remainder.slide(collision.normal)
		collision = move_and_collide(remainder)
		#These two lines are not critical, but make physics collisions a bit smoother
		if collision and collision.get_collider() is RigidBody2D:
			collision.collider.apply_central_impulse(-collision.normal * physics_push_bounce_force)
	
	#Test move 1 unit down to determine if the player is grounded
	is_grounded = true if move_and_collide(Vector2.DOWN, false, true, true) else false


func change_hat(hat=null):
	if hat:
		if hat in Globals.allHats:
			if hat in Globals.player_data["hat"]["acquiredHats"]:
				if $HatContainer.get_children().size() == 0 or $HatContainer.get_child(0).name != hat:
					print("Changing hat to '" + hat + "'")
					Globals.player_data["hat"]["activeHat"] = hat
					for child in $HatContainer.get_children():
						child.queue_free()
					hat = load("res://Scenes/Characters/Player/Hats/" + hat + ".tscn").instance()
					$HatContainer.add_child(hat)
			else:
				push_error("Hat '" + hat + "' is not yet acquired.")
		else:
			push_error("Hat '" + hat + "' does not exist")
	else:
		print("Changing hat to 'null'")
		Globals.player_data["hat"]["activeHat"] = null
		for child in $HatContainer.get_children():
			child.queue_free()
