extends Sprite
#Magnetic hat

export (float) var magnet_strength = 500
export (float, 0, 10000) var max_magnet_range = 1000


func _physics_process(_delta):
	
	var _mode = int(Globals.player_data["hat"]["isHatEnabled"]) * (Input.get_action_strength("hat_action_2") + -Input.get_action_strength("hat_action_1"))
	
	match int(_mode):
		0: #none
			$push.emitting = false
			$pull.emitting = false
		1: #pull
			$push.emitting = false
			if $pull.emitting == false:
				$pull.emitting = true
			do_magnet_stuff(1)
			
		-1: #push
			$pull.emitting = false
			if $push.emitting == false:
				$push.emitting = true
			do_magnet_stuff(-1)


func do_magnet_stuff(polarity=1):
	for magnetable in get_tree().get_nodes_in_group("magnetable"):
		var distance_to_magnetable = get_global_position().distance_to(magnetable.get_global_position())
		if distance_to_magnetable <= max_magnet_range or max_magnet_range == 0:
			var direction = (get_global_position() - magnetable.get_global_position()).normalized()
			var force = (Globals.magnet_C * magnet_strength * magnetable.magnet_strength) / pow(distance_to_magnetable, 2)
			if not Globals.is_negligible(force):
				magnetable.apply_central_impulse(force*direction*polarity)
