extends Sprite

var time_stopped = false

func _input(_event):
	if Input.is_action_just_pressed("hat_action_1"):
		if Globals.player_data["hat"]["isHatEnabled"]:
			time_stopped = !time_stopped
			for object in get_tree().get_nodes_in_group("timeStoppable"):
				object.flip_time()
#			get_tree().paused = true if time_stopped else false
			
			if !time_stopped:
				$resume.play()
				$stop.stop()
			else:
				$stop.play()
				$resume.stop()
