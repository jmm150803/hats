extends Sprite
# Tesla Hat

export (float, 0, 10000) var max_tesla_range = 200
var _last_nearest_usable_teslatable = null


func _physics_process(_delta):
	if Globals.player_data["hat"]["isHatEnabled"]:
		var nearest_usable_teslatable = _get_nearest_usable_teslatable()
		if nearest_usable_teslatable != _last_nearest_usable_teslatable:
			if _last_nearest_usable_teslatable:
				_last_nearest_usable_teslatable.set_active(false)
			if nearest_usable_teslatable:
				nearest_usable_teslatable.set_active(true)
		_last_nearest_usable_teslatable = nearest_usable_teslatable


func _input(event):
	if event.is_action_pressed("hat_action_1") and Globals.player_data["hat"]["isHatEnabled"] and _last_nearest_usable_teslatable:
		_last_nearest_usable_teslatable.do_tesla_stuff()


func _get_nearest_usable_teslatable():
	var nearest_usable_teslatable = null
	for teslatable in get_tree().get_nodes_in_group("teslatable"):
		if not nearest_usable_teslatable or teslatable.global_position.distance_to(global_position) < nearest_usable_teslatable.global_position.distance_to(global_position):
			nearest_usable_teslatable = teslatable
	
	if nearest_usable_teslatable.global_position.distance_to(global_position) < max_tesla_range or max_tesla_range == 0:
		return nearest_usable_teslatable
	else:
		return null
