#Description:
#This script is for storing global data and providing common utility functions

extends Node

const magnet_C = 256

var allHats = ["MagneticHat", "TimeHat", "GenericHat", "TeslaHat"]

var player_data = {
	"player": {"canMove":true},
	"hat": {"isHatEnabled":true, "activeHat":null, "acquiredHats":[]}
}

var debug = true

var tile_unit_size = 64

var lang = "eng"

var audio_master = 0
var audio_ambient = 0
var audio_music = 0
var audio_effects = 0


func _ready():
	print("Jsem ani nečekal, že si tohle někdo přečte... no hlavně tohle okno nezavírej, nebo ti to spadne :) \n")
	
	#center window
	var screen_size = OS.get_screen_size(0)
	var window_size = OS.get_window_size()
	OS.set_window_position(screen_size*0.5 - window_size*0.5)
	
	#make /clyde folder in appdata directory
	print("'user://clyde' folder creation code: ", AppData.create_directory("clyde"))
	
	#setup debug mode
	if debug:
		player_data["hat"]["acquiredHats"] = allHats


func _input(event):
	if event.is_action_pressed("ui_cancel"):
		print("load: res://Scenes/Other/Menu.tscn")
		# warning-ignore:return_value_discarded
		get_tree().change_scene("res://Scenes/Other/Menu.tscn")


func is_negligible(value):
	return abs(value) < 0.15


func gimme_tree():
	return get_tree()
