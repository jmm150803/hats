extends StaticBody2D

export (bool) var door_open = false

var _colors = ["ffff00", "20ffff00"]

signal door_open
signal door_close
signal door_changed(change)


func _ready():
	if door_open:
		$Sprite.modulate = _colors[int(door_open)]
		$CollisionShape2D.set_deferred("disabled", true)


func open():
	if not door_open:
		door_open = true
		emit_signal("door_open")
		emit_signal("door_changed", true)
	$Sprite.modulate = _colors[int(door_open)]
	$CollisionShape2D.set_deferred("disabled", true)


func close():
	if door_open:
		door_open = false
		emit_signal("door_close")
		emit_signal("door_changed", false)
		$Sprite.modulate = _colors[int(door_open)]
		$CollisionShape2D.set_deferred("disabled", false)


func toggle():
	door_open = !door_open
	if door_open:
		emit_signal("door_open")
	else:
		emit_signal("door_close")
	emit_signal("door_changed", door_open)
	$Sprite.modulate = _colors[int(door_open)]
	$CollisionShape2D.set_deferred("disabled", !$CollisionShape2D.disabled)
