extends Area2D

enum{Normal, Switch, SwitchSingle}
enum{Characters, Objects, Both}
export (int, "Normal", "Switch", "Switch-single") var button_mode
export (int, "Characters", "Objects", "Both") var button_triggers

signal button_pressed
signal button_released
signal button_changed(status)

var pressed = false
var _last_pressed = false
var _bodies_inside = 0
const _colors = ["ff0000", "00ff00"]


func _on_Button_body_entered(body):
	if button_triggers == Characters:
		if body.is_in_group("Characters"):
			_bodies_inside += 1
			_update_button(true)
		
	elif button_triggers == Objects:
		if body.is_in_group("Objects"):
			_bodies_inside += 1
			_update_button(true)
		
	elif button_triggers == Both:
		if body.is_in_group("Characters") or body.is_in_group("Objects"):
			_bodies_inside += 1
			_update_button(true)


func _on_Button_body_exited(body):
	if button_triggers == Normal:
		if body.is_in_group("Characters"):
			_bodies_inside -= 1
			_update_button()
		
	elif button_triggers == Switch:
		if body.is_in_group("Objects"):
			_bodies_inside -= 1
			_update_button()
		
	elif button_triggers == SwitchSingle:
		if body.is_in_group("Characters") or body.is_in_group("Objects"):
			_bodies_inside -= 1
			_update_button()


func _update_button(entering=false):
	if button_mode == 0:
		if _bodies_inside > 0:
			pressed = true
			if pressed != _last_pressed:
				emit_signal("button_pressed")
				emit_signal("button_changed", true)
		else:
			pressed = false
			if pressed != _last_pressed:
				emit_signal("button_released")
				emit_signal("button_changed", false)
		_last_pressed = pressed
	
	
	elif button_mode == 1:
		if entering:
			if _bodies_inside == 1:
				pressed = !pressed
				if pressed:
					emit_signal("button_pressed")
				else:
					emit_signal("button_released")
				emit_signal("button_changed", pressed)
	
	
	elif button_mode == 2:
		if pressed == false and _bodies_inside > 0:
			pressed = true
			if pressed != _last_pressed:
				emit_signal("button_pressed")
				emit_signal("button_changed", true)
	
	$Sprite.modulate = _colors[int(pressed)]
