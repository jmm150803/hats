extends Node2D

#TODO: udělat z toho button like switch

#export (int, "Normal", "Switch", "Switch-single") var mode
#var active = false
#
#signal switch_active
#signal switch_inactive
#signal switch_changed(status)

func _ready():
#	set_physics_process(false)
	add_to_group("teslatable")
	modulate = Color("ff0000")


func set_active(set):
	#placeholder do doby, než bude grafika (nějaký jiskry, nebo "blesky" mezi kloboukem a switchem)
	if set:
		#Tesla effect on
		modulate = Color("00ff00")
	else:
		#Tesla effect off
		modulate = Color("ff0000")


func do_tesla_stuff():
	print(str(self) + " Look mom! I am doing tesla stuff!")

#	match mode:
#		0:
#			active = true
#			emit_signal("switch_active")
#
#		1:
#			active = !active
#			emit_signal("switch_active")
#
#		2:
#			active = true
#			emit_signal("switch_active")
#
#
#
#func _physics_process(_delta):
#	if not active:
#		emit_signal("switch_inactive")

