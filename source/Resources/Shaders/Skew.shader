shader_type canvas_item;

// parameters
uniform float levelx = 0.0;
uniform float levely = -0.5;

void vertex() {
	VERTEX.x -= VERTEX.y * levelx;
	VERTEX.y -= VERTEX.x * levely;
}